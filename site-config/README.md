# Site specific config for Spack

Usually the machine name is available in an environment variable like
`$SYSTEMNAME`, `$HPC_SYSTEM` or equivalent to load local base configuration.
The environment base `spack.yaml` then includes all files in the specific
folder:

```yaml
spack:
  include:
  - site-config/$SYSTEMNAME
```

You can check the overlay is working by
```bash
spack env activate .
spack config blame config
```

Some system specific entries should be governed by the site-specific
envrionment config.
