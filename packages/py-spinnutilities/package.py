# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySpinnutilities(PythonPackage):
    """This provides basic utility functions and classes to other parts of
    SpiNNaker"s tooling. Nothing in here knows anything about SpiNNaker
    functionality."""

    homepage = "https://github.com/SpiNNakerManchester/SpiNNUtils"
    pypi = "SpiNNUtilities/SpiNNUtilities-1!7.0.0.tar.gz"

    version("7.0.0", sha256="662855395ec367008735047a66a7ca75d1e5070e309ca3aa6ba3a843fb722841")

    depends_on("python@3.7:", type=("build", "run"))
    depends_on("py-appdirs", type=("build", "run"))
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-pyyaml", type=("build", "run"))
    depends_on("py-requests", type=("build", "run"))
