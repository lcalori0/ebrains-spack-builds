# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySpinnman(PythonPackage):
    """This package provides utilities for interacting with a SpiNNaker
    machine."""

    homepage = "https://github.com/SpiNNakerManchester/SpiNNMan"
    pypi = "SpiNNMan/SpiNNMan-1!7.0.0.tar.gz"

    version("7.0.0", sha256="61bc8934e4ad6798b48c02ff6c8a3ef5c8e080a5ee2f4b88fc9cd587ed1b1ae6")

    depends_on("python@3.7:", type=("build", "run"))
    depends_on("py-spinnmachine@7.0.0", type=("build", "run"))
    depends_on("py-websocket-client", type=("build", "run"))
