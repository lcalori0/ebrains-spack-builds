# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class RRcppxptrutils(RPackage):
    """Provides the means to compile user-supplied C++ functions with 'Rcpp' and retrieve an 'XPtr' that can be passed to other C++ components."""

    homepage = "https://cran.r-project.org/package=RcppXPtrUtils"
    cran = "RcppXPtrUtils"

    version("0.1.2", sha256="34d0cab1891c95453c357cf93ab97b6ea42ef98cdc4809c9abfafd469c3da542")

    depends_on("r-rcpp")
