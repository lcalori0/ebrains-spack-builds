# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyPyspike(PythonPackage):
    """
    PySpike is a Python library for the numerical analysis of spike train similarity.
    """

    homepage = "https://github.com/mariomulansky/PySpike"
    pypi = 'pyspike/pyspike-0.7.0.tar.gz'

    maintainers = ['dionperd', 'paulapopa', "ldomide"]

    version('0.8.0', '76137b861ed531608aaf55af1a5ebf8a586e98653dab2467b4c1da7b2d9aa4e5')
    version('0.7.0', 'a5d1c1472d3e7c3ac85c8a4ce069d750cca02acf18f185677b29c0a757e78efe')

    # python_requires
    depends_on('python@3.8:3.10', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type='build')
    depends_on('py-setuptools', type=('build'))

    # install_requires
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-matplotlib', type=('build', 'run'))
    depends_on('py-pytest', type=('build', 'run'))
    depends_on('py-cython@:2', type=('build', 'run'))

    # Test dependency
    depends_on('py-pytest@:7.1', type='test')
    
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest()
