# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBsb(PythonPackage):
    """
    The BSB is a component framework for neural modeling, which focuses on component
    declarations to piece together a model.
    """

    homepage = "https://bsb.readthedocs.io"
    url = "https://pypi.org/packages/py3/b/bsb/bsb-4.0.0a57-py3-none-any.whl"

    maintainers = ["helveg"]

    version("4.0.0a57", sha256="5da15799aa8994894ff5371561d534b43beffaa79461189c94080071359f4076", expand=False)

    depends_on("python@3.8:", type=("build", "run"))
    depends_on("py-setuptools", type="build")
    depends_on("py-numpy@1.19:")
    depends_on("py-scipy@1.5:")
    depends_on("py-scikit-learn@1.0:")
    depends_on("py-plotly")
    depends_on("py-rtree@1.0:")
    depends_on("py-psutil@5.8:")
    depends_on("py-pynrrd@1.0:")
    depends_on("py-toml")
    depends_on("py-requests")
    depends_on("py-appdirs@1.4:")
    depends_on("py-neo")
    depends_on("py-tqdm@4.50:")
    depends_on("py-shortuuid")
    depends_on("py-quantities")
    depends_on("py-pyyaml@6.0:")
    depends_on("py-morphio@3.3:")
    depends_on("py-bsb-hdf5@0.8.3:")
    depends_on("py-errr@1.2.0:")
    depends_on("py-colour@0.1.5:")

    def setup_build_environment(self, env):
        env.set("SPATIALINDEX_C_LIBRARY", self.spec["libspatialindex"].libs[0])

    def setup_run_environment(self, env):
        self.setup_build_environment(env)

    skip_modules = ['bsb.simulators.arbor', 'bsb.simulators.arbor.devices']
    
