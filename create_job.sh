#!/bin/bash

# ===========================================================================================================
# title         : create_job.sh
# usage         : ./create_job.sh $OC_JOB_ID $BUILD_ENV_DOCKER_IMAGE $OP $INSTALLATION_ROOT $SPACK_VERSION
#                 $SPACK_ENV $BRANCH $RELEASE_NAME $LAB_KERNEL_ROOT
# description   : creates OKD job yaml file that builds/updates spack environment and creates Lab kernel
# ===========================================================================================================

OC_JOB_ID=$1
BUILD_ENV_DOCKER_IMAGE=$2
OP=$3
INSTALLATION_ROOT=$4
SPACK_VERSION=$5
SPACK_ENV=$6
BRANCH=$7
RELEASE_NAME=$8
LAB_KERNEL_ROOT=$9

cat <<EOT >> simplejob.yml
apiVersion: batch/v1
kind: Job
metadata:
  name: simplejob${OC_JOB_ID}
spec:
  parallelism: 1
  completions: 1
  backoffLimit: 0
  template:
    metadata:
      name: testjob
    spec:
      containers:
      - name: simplejob
        image: ${BUILD_ENV_DOCKER_IMAGE}
        imagePullPolicy: Always
        volumeMounts:
          - name: sharedbin
            mountPath: /srv
        command: ["/usr/local/bin/deploy-build-env.sh", "$OP", "$INSTALLATION_ROOT", "$SPACK_VERSION", "$SPACK_ENV", "$BRANCH", "$RELEASE_NAME", "$LAB_KERNEL_ROOT"]
        env:
          - name: SYSTEMNAME
            value: ebrainslab
          - name: GITLAB_USER
            valueFrom:
              secretKeyRef:
                name: spack-repo-gitlab-token
                key: username
          - name: GITLAB_TOKEN
            valueFrom:
              secretKeyRef:
                name: spack-repo-gitlab-token
                key: password
      volumes:
        - name: sharedbin
          persistentVolumeClaim:
            claimName: shared-binaries
      restartPolicy: Never
EOT
